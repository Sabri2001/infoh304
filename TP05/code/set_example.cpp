#include <iostream>
#include <string>
#include <set>
#include <fstream>
using namespace std;

int printSet(const set<string> & mots, ostream & out = cout)
{
	set<string>::const_iterator courant;
	for(courant = mots.begin(); courant != mots.end(); courant++)
	{
		out << *courant << endl;
	}
}

int main(int argc, char *argv[])
{
	
	pair<set<string>::iterator,bool> ret;
	set<string> ensemble;
	
	int counter = 0;
	
	ifstream fichier ("./sherlock.txt");
	
	if( fichier.is_open() )
	{
		string mot;
		cout << "Fichier ouvert" << endl;
		while( fichier >> mot )
		{
			//cout << "Mot: " << mot << endl;
			//cout << "1" << endl;
			ret = ensemble.insert(mot);
			//cout << "2" << endl;
			//cout << "Ret second: " << ret.second << endl;
			if(ret.second)
			{
				counter++;
			}
		}
		fichier.close();
	}
	else
		cout << "Impossible d'ouvrir le fichier" << endl;
		
	printSet(ensemble);
	cout << "Nombre de mots differents dans le fichier: " << counter << endl;
}
