#include <iostream>
#include <string>
#include <map>
#include <fstream>
using namespace std;

int printDict(map<string,int> & mots, ostream & out = cout)
{
	map<string,int>::const_iterator courant;
	for(courant = mots.begin(); courant != mots.end(); courant++)
	{
		out << courant->first << ": "<< courant->second<< endl;
	}
	return 0;
}

int writeDict(map<string,int> & mots, ostream & out = cout)
{
	map<string,int>::const_iterator courant;
	for(courant = mots.begin(); courant != mots.end(); courant++)
	{
		out<< courant->first << ": "<< courant->second<< endl;
	}
	return 0;
}

int main(int argc, char *argv[])
{
	map<string,int> dictionnaire;
	pair<map<string,int>::iterator, bool> ret;
	ifstream fichier (argv[1]);
	
	if( fichier.is_open() )
	{
		string mot;
		cout << "Fichier ouvert" << endl;
		while( fichier >> mot )
		{
			ret = dictionnaire.insert(pair<string,int>(mot,1));
			if(not ret.second)
			{
				dictionnaire[mot]++;
			}
		}
		
		fichier.close();
	}
	else
		cout << "Impossible d'ouvrir le fichier" << endl;
		
	ofstream outfile;
	outfile.open("sherlock_dico.txt");
	printDict(dictionnaire, outfile);
}
